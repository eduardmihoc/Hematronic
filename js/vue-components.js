const routes = ['homepage', 'education', 'products', 'career', 'contact'];
const navigationListTranslation = {
    ro: {
        homepage: 'Prezentare',
        ecm: 'Educatie medicala continua',
        products: 'Produse',
        contact: 'Contact',
        language: '<span class="flag-icon flag-icon-ro lead"></span>'
    },
    en_us: {
        homepage: 'Home',
        ecm: 'Continuous medical education',
        products: 'Products',
        contact: 'Contact',
        language: '<span class="flag-icon flag-icon-us lead"></span>'
    },
    fr_fr: {
        homepage: 'Maison',
        ecm: 'Formation médicale continue\n',
        products: 'Produits',
        contact: 'Contact',
        language: '<span class="flag-icon flag-icon-fr lead"></span>'
    }
};

Vue.component('navigation-bar', {
    props: ['myData'],
    data: function () {
        return {
            currentPage: 'homepage',
            navigationItems: navigationListTranslation.ro
        }
    },
    template: '<!--Navbar-->\n' +
    '        <nav class="navbar navbar-expand-md navbar-dark fixed-top scrolling-navbar dark-background">\n' +
    '            <div class="container">\n' +
    '                <a class="navbar-brand logo" href="#" v-on:click="navigationEvent(0)">Hematronic</a>\n' +
    '                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">\n' +
    '                    <span class="navbar-toggler-icon"></span>\n' +
    '                </button>\n' +
    '                <div class="collapse navbar-collapse" id="navbarSupportedContent">\n' +
    '                    <ul class="navbar-nav mr-auto navbar-override pr-4">\n' +
    '                        <li class="nav-item" v-bind:class="{active: currentPage === \'homepage\'}">\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(0)" >{{navigationItems.homepage}} <span class="sr-only">(current)</span></a>\n' +
    '                        </li>\n' +
    '                        <li class="nav-item" v-bind:class="{active: currentPage === \'education\'}">\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(1)">{{navigationItems.ecm}}</a>\n' +
    '                        </li>\n' +
    '                        <li class="nav-item" v-bind:class="{active: currentPage === \'products\'}">\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(2)">{{navigationItems.products}}</a>\n' +
    '                        </li>\n' +
    /*    '                        <li class="nav-item" v-bind:class="{active: currentPage === \'career\'}">\n' +
        '                            <a class="nav-link" href="#" v-on:click="navigationEvent(3)">Cariere</a>\n' +
        '                        </li>\n' +*/
    '                        <li class="nav-item mr-2" v-bind:class="{active: currentPage === \'contact\'}">\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(4)">{{navigationItems.contact}}</a>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                    <form class="form-inline language-container">\n' +
    '                        <li class="nav-item btn-group">\n' +
    '                           <div class="dropdown background-azure">\n' +
    '                               <button class="btn btn-primary dropdown-toggle language-selector" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-html="navigationItems.language">\n' +
    '                               </button>\n' +
    '                               <div class="dropdown-menu language-picker-position" aria-labelledby="dropdownMenu3">\n' +
    '                                   <a class="dropdown-item" href="#/" v-on:click="languageSwitcher(\'ro\')"><span class="flag-icon flag-icon-ro"></span> Romana</a>\n' +
    '                                   <a class="dropdown-item" href="#/" v-on:click="languageSwitcher(\'en-us\')"><span class="flag-icon flag-icon-us"></span> English</a>\n' +
    '                                   <a class="dropdown-item" href="#/" v-on:click="languageSwitcher(\'fr-fr\')"><span class="flag-icon flag-icon-fr"></span> Français</a>\n' +
    '                               </div>\n' +
    '                           </div>' +
    '                        </li>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </nav>\n' +
    '    <!--/.Navbar-->',
    methods: {
        navigationEvent: function (param) {
            this.currentPage = routes[param];
            this.$root.navigationHandler(param);
        },
        languageSwitcher: function (param) {
            this.navigationItems = navigationListTranslation[param.replace('-', '_')];
            this.$root.languageSwitcher(param);
        }
    }
});

Vue.component('carousel-wrapper', {
    props: ['myData'],
    template: '<!--Carousel Wrapper-->\n' +
    '    <div id="carousel-example-3" class="carousel slide carousel-fade white-text" data-ride="carousel" data-interval="false">\n' +
    '        <!--Indicators-->\n' +
    '        <ol class="carousel-indicators">\n' +
    // '            <li data-target="#carousel-example-3" data-slide-to="0" class="active"></li>\n' +
    /*    '            <li data-target="#carousel-example-3" data-slide-to="1"></li>\n' +
        '            <li data-target="#carousel-example-3" data-slide-to="2"></li>\n' +*/
    '        </ol>\n' +
    '        <!--/.Indicators-->\n' +
    '\n' +
    '        <!--Slides-->\n' +
    '        <div class="carousel-inner" role="listbox">\n' +
    '\n' +
    '            <!-- First slide -->\n' +
    '            <div class="carousel-item active view hm-black-light" v-bind:style="{ backgroundImage: \'url(\' +  myData.image + \')\', backgroundRepeat: \'no-repeat\', backgroundSize: \'cover\'}" style="">\n' +
    '\n' +
    '                <!-- Caption -->\n' +
    '                <div class="full-bg-img flex-center white-text">\n' +
    '                    <ul class="carousel-container-ul animated fadeInUp col-md-12">\n' +
    '                       <li class="carousel-body maine-item pb-0" v-bind:class="{desktopCarousel: myData.showMobile, hidePanels: !myData.imageLeft.length || !myData.imageRight.length}">' +
    '                           <div class="carousel-image-left" v-if="myData.showMobile" v-bind:style="{ backgroundImage: \'url(\' +  myData.imageLeft + \')\'}"></div>' +
    '                           <span v-html="myData.body" class="carousel-data">' +
    '<div class="row"></div>' +
    '</span>' +
    // '                           <div class="carousel-image-right" v-if="myData.showMobile" v-bind:style="{ backgroundImage: \'url(\' +  myData.imageRight + \')\'}"></div>' +
    '                       </li>' +
    '                <div class="carousel-text-bottom carousel-body pt-0" v-bind:class="{desktopCarouselSecond: myData.showMobile}">' +
    '                   <span v-html="myData.bodySecondary" class="carousel-data"></span>' +
    '                </div>' +

    '                    </ul>\n' +
    '                </div>\n' +
    '                <!-- /.Caption -->\n' +
    '\n' +
    '            </div>\n' +
    '            <!-- /.First slide -->\n' +
    '\n' +
    '            \n' +
    '        </div>\n' +
    '        <!--/.Slides-->\n' +
    '\n' +
    '    </div>\n' +
    '    <!--/.Carousel Wrapper-->'
});
Vue.component('material-footer', {
    props: ['myData'],
    data: function () {
        return {
            currentPage: 'homepage',
            navigationItems: navigationListTranslation.ro
        }
    },
    template: '<footer class="page-footer center-on-small-only mt-0">\n' +
    '\n' +
    '        <!--Footer links-->\n' +
    '        <div class="container-fluid">\n' +
    '            <div class="row">\n' +
    '                <!--First column-->\n' +
    '                <div class="col-lg-4 col-md-6 ml-auto">\n' +
    '                    <h5 class="title mb-3"><strong>{{myData.aboutTitle}}</strong></h5>\n' +
    '                    <p>{{myData.aboutDescription}}</p>\n' +
    '                </div>\n' +
    '                <!--/.First column-->\n' +
    '                <hr class="w-100 clearfix d-sm-none">\n' +
    '                <!--Second column-->\n' +
    '                <div class="col-lg-3 col-md-6 ml-auto">\n' +
    '                    <h5 class="title mb-3"><strong>{{myData.section_navigation.sectionTitle}}</strong></h5>\n' +
    '                    <ul>\n' +
    '                        <li>\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(0)" >{{navigationItems.homepage}} <span class="sr-only">(current)</span></a>\n' +
    '                        </li>\n' +
    '                        <li>\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(1)">{{navigationItems.ecm}}</a>\n' +
    '                        </li>\n' +
    '                        <li>\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(2)">{{navigationItems.products}}</a>\n' +
    '                        </li>\n' +
    '                        <li>\n' +
    '                            <a class="nav-link" href="#" v-on:click="navigationEvent(4)">{{navigationItems.contact}}</a>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                </div>\n' +
    '                <!--/.Second column-->\n' +
    '                <hr class="w-100 clearfix d-sm-none">\n' +
    '                <!--Third column-->\n' +
    '                <div class="col-lg-3 col-md-6 ml-auto">\n' +
    '                    <h5 class="title mb-3"><strong>{{myData.useful_links.sectionTitle}}</strong></h5>\n' +
    '                    <ul>\n' +
    '                        <li v-for="item in myData.useful_links.sectionLinks">\n' +
    '                            <a v-bind:href="item.url" target="_blank">{{item.label}}</a>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                </div>\n' +
    '                <!--/.Third column-->\n' +
    // '                <hr class="w-100 clearfix d-sm-none">\n' +
    '                <!--Fourth column-->\n' +
    /*    '                <div class="col-lg-2 col-md-6 ml-auto">\n' +
        '                    <h5 class="title mb-3"><strong>{{myData.documentation_links.sectionTitle}}</strong></h5>\n' +
        '                    <ul>\n' +
        '                        <li v-for="item in myData.documentation_links.sectionLinks">\n' +
        '                            <a v-bind:href="item.url" target="_blank">{{item.label}}</a>\n' +
        '                        </li>\n' +
        '                    </ul>\n' +
        '                </div>\n' +*/
    '                <!--/.Fourth column-->\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--/.Footer links-->\n' +
    '\n' +
    '\n' +
    '\n' +
    '        <!--Copyright-->\n' +
    '        <div class="footer-copyright">\n' +
    '            <div class="container-fluid">\n' +
    '                © Web-dev: <a href="https://www.facebook.com/eduardmihoc"> Eduard Mihoc </a>\n' +
    '\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!--/.Copyright-->\n' +
    '\n' +
    '    </footer>\n' +
    '    <!--/.Footer-->',
    methods: {
        navigationEvent: function (param) {
            this.currentPage = routes[param];
            this.$root.navigationHandler(param);
        }
    }
});

const NotFound = {template: '<p>Page not found</p>'};
