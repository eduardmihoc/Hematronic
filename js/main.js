var PrismicRepo = 'https://hematronic.prismic.io/api/v2';

(function (win, doc, Vue, Prismic, Email, PrismicDOM) {
    let homePageData = [],
        educationData = [],
        productData = [],
        contactData = [];

    const languagePlaceHolders = {
        ro: {
            name: 'Nume si prenume',
            email: ' Adresa de e-mail',
            phone: ' Numar Telefon',
            attendees: 'Nr. persoane curs',
            location: 'Oras/Spital',
            message: 'Mesaj...',
            sendLabel: 'Trimite'
        },
        en_us: {
            name: 'Name and surname',
            email: 'E-mail address',
            phone: 'Phone no.',
            attendees: 'No. attendees',
            location: 'Town/Hospital',
            message: 'Message...',
            sendLabel: 'Send'
        },
        fr_fr: {
            name: 'Name and surname',
            email: 'E-mail address',
            phone: 'Phone no.',
            attendees: 'No. attendees',
            location: 'Town/Hospital',
            message: 'Message...',
            sendLabel: 'Send'
        }
    };

    function isMobileDevice() {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    };

    function getHematronicContent(documentSelector, documentIdentifier, additionalParams) {
        return Prismic.api(PrismicRepo).then(function (api) {
            return api.query(
                Prismic.Predicates.at('document.' + documentSelector, documentIdentifier),
                additionalParams
            )
        });
    }

    function filter(text, length, clamp) {
        clamp = clamp || '...';
        let node = document.createElement('div');
        node.innerHTML = text;
        let content = node.textContent;
        return content.length > length ? content.slice(0, length) + clamp : content;
    }

    function parsePrismicData(prismicArray) {
        let parsedData = [];
        jQuery.each(prismicArray, function (index, value) {
            parsedData.push(value.rawJSON);
        });
        return parsedData;
    }

    function getDataForLanguage(language) {
        language = language || 'ro';

        getHematronicContent('type', 'web_structure', {lang: language}).then(function (response) {
            let rawStructure = response.results[0].rawJSON;
            win.app.backgroundConfigs.education = rawStructure.education_background.url;
            win.app.backgroundConfigs.products = rawStructure.product_background.url;
            win.app.backgroundConfigs.career = rawStructure.carreer_background.url;
            win.app.backgroundConfigs.contact = rawStructure.contact_background.url;

            win.app.educationConfigs.imageOne = rawStructure.education_image_one.url;
            win.app.educationConfigs.imageTwo = rawStructure.education_image_two.url;
        });

        getHematronicContent('type', 'homepage_carousel', {lang: language}).then(function (response) {
            win.app.carouselItem.title = response.results[0].rawJSON.carousel_title[0].text || '';
            win.app.carouselItem.image = response.results[0].rawJSON.carousel_image.url || '';
            win.app.carouselItem.imageLeft = response.results[0].rawJSON.small_image_left.url || '';
            win.app.carouselItem.imageRight = response.results[0].rawJSON.small_image_right.url || '';
            win.app.carouselItem.body = PrismicDOM.RichText.asHtml(response.results[0].rawJSON.carousel_text || '');
            win.app.carouselItem.bodySecondary = PrismicDOM.RichText.asHtml(response.results[0].rawJSON.carousel_text_secondary || '');
        }).then(function (response) {
            jQuery('.homepage').height(jQuery('.carousel-container-ul').height() * 1.3);
            jQuery('.carousel-container-ul').addClass('show');
        });

        getHematronicContent('type', 'education_item', {lang: language}).then(function (response) {
            win.app.educationItems = [];

            jQuery.each(parsePrismicData(response.results), function (index, value) {
                if (index !== (length - 1)) {
                    win.app.educationItems.push({
                        title: value.education_item_title[0].text || '',
                        category: value.education_item_category[0].text || '',
                        cta: value.education_item_cta[0].text || '',
                        description: PrismicDOM.RichText.asHtml(value.education_item_description || ''),
                        image: value.education_item_image.url || ''
                    });
                }
            });
        });

        getHematronicContent('type', 'product', {lang: language}).then(function (response) {
            win.app.productItems = [];
            jQuery.each(parsePrismicData(response.results), function (index, value) {
                let productDocument = PrismicDOM.Link.url(value.product_document);
                if (index !== (length - 1)) {
                    win.app.productItems.push({
                        title: value.product_title[0].text,
                        category: value.product_category[0].text,
                        description: value.product_description[0].text,
                        image: value.product_image.url,
                        document: productDocument || '',
                        docTitle: value.product_document_title.length ? value.product_document_title[0].text : 'Document',
                        price: value.product_price[0].text
                    });
                }
            });
        });

        getHematronicContent('type', 'contact_info', {lang: language}).then(function (response) {
            let emcData = (response.results[0]['uid'] === "contact_emc_" + language.replace('-', '_')) ? response.results[0].rawJSON : response.results[1].rawJSON;
            let officeData = (response.results[0]['uid'] === "contact_office_" + language.replace('-', '_')) ? response.results[0].rawJSON : response.results[1].rawJSON;
            /*this.language.replace('-', '_')*/
            win.app.contactInfoEMC = {
                contact_title: emcData.contact_title[0].text,
                contact_info: emcData.contact_information[0].text,
                contact_message: emcData.contact_message[0].text,
                contact_address: emcData.contact_address[0].text,
                contact_email: emcData.contact_email[0].text,
                contact_phone_number: emcData.contact_phone_number[0].text,
                contact_facebook: emcData.contact_facebook[0].text,
                contact_twitter: emcData.contact_twitter[0].text,
                contact_linkedin: emcData.contact_linkedin[0].text
            };

            win.app.contactInfoOffice = {
                contact_title: officeData.contact_title[0].text,
                contact_info: officeData.contact_information[0].text,
                contact_message: officeData.contact_message[0].text,
                contact_address: officeData.contact_address[0].text,
                contact_email: officeData.contact_email[0].text,
                contact_phone_number: officeData.contact_phone_number[0].text,
                contact_facebook: officeData.contact_facebook[0].text,
                contact_twitter: officeData.contact_twitter[0].text,
                contact_linkedin: officeData.contact_linkedin[0].text
            };

        });

        getHematronicContent('type', 'footer_entity', {lang: language}).then(function (response) {
            let footerEntity = response.results[0].rawJSON;

            win.app.footerEntity.aboutTitle = footerEntity.about[0].text;
            win.app.footerEntity.aboutDescription = footerEntity.about_description[0].text;

            jQuery.each(footerEntity.body, function (index, value) {
                let appFooterKey = value['slice_type'];
                win.app.footerEntity[appFooterKey].sectionLinks = [];
                win.app.footerEntity[appFooterKey].sectionTitle = value.primary.section_header[0]['text'] || '';
                if (index !== (length - 1)) {
                    // footerEntity[appFooterKey]
                    jQuery.each(value.items, function (index, item) {
                        win.app.footerEntity[appFooterKey].sectionLinks.push({
                            label: item.section_label[0].text,
                            url: item.section_link.url
                        })
                    })
                }
            });
        });

    }

    jQuery(doc).ready(function () {
        Vue.filter('truncate', filter);

        win.app = new Vue({
            el: '.app',
            data: {
                isMobile: isMobileDevice(),
                currentView: 'homepage',
                carouselItem: {
                    image: '',
                    imageLeft: '',
                    imageRight: '',
                    title: '',
                    body: '',
                    bodySecondary: '',
                    showMobile: !isMobileDevice()
                },
                productItems: [],
                educationItems: [],
                educationConfigs: {
                    imageOne: '',
                    imageTwo: ''
                },
                backgroundConfigs: {
                    currentBackground: '',
                    education: '',
                    products: '',
                    career: '',
                    contact: ''
                },
                contactInfoEMC: {},
                contactInfoOffice: {},
                formDataPlaceHolder: languagePlaceHolders.ro,
                ecmFormData: {
                    show: true
                },
                officeFormData: {
                    show: false
                },
                footerEntity: {
                    aboutTitle: '',
                    aboutDescription: '',
                    section_navigation: {
                        sectionTitle: '',
                        sectionLinks: []
                    },
                    useful_links: {
                        sectionTitle: '',
                        sectionLinks: []
                    },
                    documentation_links: {
                        sectionTitle: '',
                        sectionLinks: []
                    }
                }
            },
            methods: {
                navigationHandler: function (param) {
                    this.currentView = routes[param];
                    this.backgroundConfigs.currentBackground = routes[param] === 'homepage' ? this.carouselItem.image : this.backgroundConfigs[routes[param]];
                },
                languageSwitcher: function (param) {
                    this.language = param;
                    this.formDataPlaceHolder = languagePlaceHolders[this.language.replace('-', '_')];
                    getDataForLanguage(this.language);
                },
                sendEMCData: function () {
                    Email.send(this.ecmFormData.email,
                        "emc@hematronic.ro",
                        "Educatie Medicala continue - " + this.ecmFormData.location + ' - ' + this.ecmFormData.attendees + ' participanti',
                        this.ecmFormData.message.replace(/\r?\n/g, "<br />"),
                        {
                            token: "84d75a5d-9ac5-43e7-9997-25df7c402e88",
                            callback: function done(message) {
                                alert(message)
                            }
                        }
                    );
                },
                sendOfficeData: function () {
                    Email.send(this.officeFormData.email,
                        "office@hematronic.ro",
                        "Educatie Medicala continue - " + this.officeFormData.location + ' - ' + this.officeFormData.attendees + ' participanti',
                        this.officeFormData.message.replace(/\r?\n/g, "<br />"),
                        {
                            token: "68fbb7ba-6a8b-4265-9c36-8bd74e83e369",
                            callback: function done(message) {
                                alert(message)
                            }
                        }
                    );
                },
                formHandler: function (param) {
                    if (param === 'emc') {
                        this.ecmFormData.show = true;
                        this.officeFormData.show = false;
                    } else if (param === 'office') {
                        this.ecmFormData.show = false;
                        this.officeFormData.show = true;
                    }
                },
                onResize: function () {
                    if (document.documentElement.clientWidth < 800) {
                        this.isMobile = true;
                        this.carouselItem.showMobile = false;
                    } else {
                        this.isMobile = false;
                        this.carouselItem.showMobile = true;
                    }
                }
            },
            beforeCreate: function () {
                getDataForLanguage(this.language);
            },
            mounted: function () {
                let self = this;
                // Register an event listener when the Vue component is ready
                window.addEventListener('resize', this.onResize);
                if (self.currentView === 'homepage') {
                    setTimeout(function () {
                        jQuery('.homepage').height(jQuery('.carousel-container-ul').height() * 1.3);
                        jQuery('.carousel-container-ul').addClass('show');
                    }, 1500)
                }
            },
            beforeDestroy: function () {
                // Unregister the event listener before destroying this Vue instance
                window.removeEventListener('resize', this.onResize)
            },
            updated: function () {
                jQuery('.carousel-container-ul').addClass('show');
            }

        });


    })//ready
}(window, document, Vue, Prismic, Email, PrismicDOM));


